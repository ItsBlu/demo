﻿using Demo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Demo.ViewModel
{
    public class AddUsersVM : INotifyPropertyChanged
    {
        #region NotifyProperty
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private string _firstname { get; set; }
        public string Firstname
        {
            get { return _firstname; }
            set
            {
                _firstname = value;
                NotifyPropertyChanged();
            }
        }
        private string _lastname { get; set; }
        public string Lastname
        {
            get { return _lastname; }
            set
            {
                _lastname = value;
                NotifyPropertyChanged();
            }
        }
        private string _email { get; set; }
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyPropertyChanged();
            }
        }
        private string _uploadStatus { get; set; }
        public string UploadStatus
        {
            get { return _uploadStatus; }
            set
            {
                _uploadStatus = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand SubmitDetailsCommand { get; set; }
        public AddUsersVM()
        {
            SubmitDetailsCommand = new Command(SubmitDetails);
        }

        private async void SubmitDetails()
        {
            if (string.IsNullOrWhiteSpace(Firstname) || string.IsNullOrWhiteSpace(Lastname) || string.IsNullOrWhiteSpace(Email))
            {
                UploadStatus = "Fill the above details before submitting.";
                return;
            }
            try
            {
                HttpClient client = new HttpClient();
                string url = "https://cila-test-api.herokuapp.com/v1/users";

                UserDetails userDetails = new UserDetails { Firstname = Firstname, Lastname = Lastname, Email = Email };
                string serializedDto = JsonConvert.SerializeObject(userDetails);
                StringContent stringContent = new StringContent(serializedDto, Encoding.UTF8, "application/json");
                HttpResponseMessage responce = await client.PostAsync(url, stringContent);
                if (responce.IsSuccessStatusCode)
                {
                    string json = await responce.Content.ReadAsStringAsync();
                    Firstname = "";
                    Lastname = "";
                    Email = "";
                    UploadStatus = "User details uploaded successfully!!!";
                }
                else
                {
                    UploadStatus = "There was an error while uploading User details";
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
    }
}