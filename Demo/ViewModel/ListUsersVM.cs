﻿using Demo.Models;
using Demo.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Demo.ViewModel
{
    public class ListUsersVM : INotifyPropertyChanged
    {
        #region NotifyProperty
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private ObservableCollection<UserDetails> _allUserDetails { get; set; }
        public ObservableCollection<UserDetails> AllUserDetails
        {
            get { return _allUserDetails; }
            set
            {
                _allUserDetails = value;
                NotifyPropertyChanged();
            }
        }
        public INavigation Navigation;

        public ICommand AddUserDetailsCommand { get; set; }
        public ICommand RefreshUserListCommand { get; set; }
        
        public ListUsersVM() { }
        public ListUsersVM(INavigation navigation)
        {
            AddUserDetailsCommand = new Command(AddUserDetails);
            RefreshUserListCommand = new Command(GetUserData);
            GetUserData();
            Navigation = navigation;
        }

        private async void AddUserDetails()
        {
            var addUserPage = new AddUsers();
            await Navigation.PushAsync(addUserPage);
        }
        private void GetUserData()
        {
            HttpClient client = new HttpClient();
            string url = "https://cila-test-api.herokuapp.com/v1/users";
            try
            {
                AllUserDetails = new ObservableCollection<UserDetails>();

                HttpResponseMessage responce = client.GetAsync(url).Result;
                if (responce.IsSuccessStatusCode)
                {
                    string json = responce.Content.ReadAsStringAsync().Result;
                    var userDetailsData = JsonConvert.DeserializeObject<UserDetailsData>(json);
                    AllUserDetails = new ObservableCollection<UserDetails>(userDetailsData.Data);
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
    }
}