﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Models
{
    public class UserDetails
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }

    public class UserDetailsData
    {
        public UserDetailsData()
        {
            Data = new List<UserDetails>();
        }
        public IList<UserDetails> Data { get; set; }
    }
}
